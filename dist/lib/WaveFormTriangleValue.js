"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WaveFormBaseValue_1 = require("./WaveFormBaseValue");
class WaveFormTriangleValue extends WaveFormBaseValue_1.default {
    constructor(poolInterval, amplitude = 1, step = 1, start = 0) {
        super(poolInterval, step, start);
        this.amplitude = amplitude;
    }
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    read() {
        return super.read().then((value) => {
            if (Math.abs(value) >= this.amplitude) {
                this.step = this.step * -1;
            }
            return value;
        });
    }
}
exports.default = WaveFormTriangleValue;
//# sourceMappingURL=WaveFormTriangleValue.js.map