"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Logger_1 = require("./Logger");
const quodii_lib_1 = require("quodii-lib");
class WaveFormBaseValue extends quodii_lib_1.PoolingSensorValue {
    constructor(poolInterval, step = 1, start = 0) {
        super(poolInterval);
        this.start = start;
        this.step = step;
        this.logger = new Logger_1.default('WaveFormBaseValue');
    }
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    read() {
        if (this.counter === undefined) {
            this.counter = this.start;
        }
        else {
            this.counter += this.step;
        }
        this.logger.debug(this.counter, this.step);
        return Promise.resolve(this.counter);
    }
}
exports.default = WaveFormBaseValue;
//# sourceMappingURL=WaveFormBaseValue.js.map