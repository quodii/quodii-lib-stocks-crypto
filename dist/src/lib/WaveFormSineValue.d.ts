import WaveFormBaseValue from './WaveFormBaseValue';
export default class WaveFormSineValue extends WaveFormBaseValue {
    protected amplitude: number;
    constructor(poolInterval: number, amplitude?: number, step?: number, start?: number);
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    protected read(): Promise<number>;
}
