import { PoolingSensorValue } from 'quodii-lib';
export default class CryptoDevice extends PoolingSensorValue {
    private url;
    private logger;
    constructor(symbols: string[], currencies?: string[], poolInterval?: number);
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    protected read(): Promise<any>;
}
