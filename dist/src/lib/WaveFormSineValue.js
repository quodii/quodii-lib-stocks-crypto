"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const WaveFormBaseValue_1 = __importDefault(require("./WaveFormBaseValue"));
class WaveFormSineValue extends WaveFormBaseValue_1.default {
    constructor(poolInterval, amplitude = 1, step = 1, start = 0) {
        super(poolInterval, step, start);
        this.amplitude = amplitude;
    }
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    read() {
        return super.read()
            .then((value) => (Math.sin(value * 0.017453301) * this.amplitude));
    }
}
exports.default = WaveFormSineValue;
//# sourceMappingURL=WaveFormSineValue.js.map