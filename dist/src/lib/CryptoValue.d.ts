import { EventEmitterSensorValue } from 'quodii-lib';
import CryptoDevice from './CryptoDevice';
export default class CryptoValue extends EventEmitterSensorValue {
    symbol: string;
    currency: string;
    constructor(device: CryptoDevice, symbol: string, currency?: string);
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    protected map(data: any): any;
}
