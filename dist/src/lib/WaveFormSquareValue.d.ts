import WaveFormSawtoothValue from './WaveFormSawtoothValue';
export default class WaveFormSquareValue extends WaveFormSawtoothValue {
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    protected read(): Promise<number>;
}
