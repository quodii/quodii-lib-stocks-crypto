"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const quodii_lib_1 = require("quodii-lib");
class CryptoValue extends quodii_lib_1.EventEmitterSensorValue {
    constructor(device, symbol, currency = 'USD') {
        super(device);
        this.symbol = symbol;
        this.currency = currency;
    }
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    map(data) {
        return data[this.symbol][this.currency];
    }
}
exports.default = CryptoValue;
//# sourceMappingURL=CryptoValue.js.map