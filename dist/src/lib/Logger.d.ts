import debug from 'debug';
export default class Logger {
    debug: debug;
    info: debug;
    warn: debug;
    error: debug;
    log: debug;
    constructor(name: string);
}
