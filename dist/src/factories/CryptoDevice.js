"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const CryptoDevice_1 = __importDefault(require("../lib/CryptoDevice"));
module.exports = () => {
    return (data) => {
        const { symbols = [], currencies = ['USD'], poolInterval = 30000 } = data || {};
        return new CryptoDevice_1.default(symbols, currencies, poolInterval);
    };
};
//# sourceMappingURL=CryptoDevice.js.map