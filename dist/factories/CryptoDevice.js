"use strict";
const CryptoDevice_1 = require("../lib/CryptoDevice");
module.exports = () => {
    return (data) => {
        const { symbols = [], currencies = ['USD'], poolInterval = 30000 } = data || {};
        return new CryptoDevice_1.default(symbols, currencies, poolInterval);
    };
};
//# sourceMappingURL=CryptoDevice.js.map