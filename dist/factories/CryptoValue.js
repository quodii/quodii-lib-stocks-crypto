"use strict";
const CryptoValue_1 = require("../lib/CryptoValue");
const assert = require("assert");
module.exports = () => {
    return (data) => {
        const { device = null, symbol = null, currency = 'USD' } = data || {};
        assert(device, 'CryptoValue Error: "device" should be defined');
        assert(symbol, 'CryptoValue Error: "symbol" should be defined');
        return new CryptoValue_1.default(device, symbol, currency);
    };
};
//# sourceMappingURL=CryptoValue.js.map