declare module 'quodii-lib' {
  export interface Message { type: string, data: any }

  export class Observer {
    public notify(event: Message)
  }

  export class Value {
    public lastValue: any

    constructor(initialValue: any, valueForwarder: Function)

    public set(value: any): void
    public get(): any
  }

  export class PoolingSensorValue extends Value {
    constructor(poolInterval: number)
  }

  export class FakeSensorValue extends Value {
    constructor(poolInterval: number, max: number, min?: number)
  }

  export class EventEmitterSensorValue extends Value {
    constructor(eventEmitter: any, eventName?: string)
    public set(value: any): void
    public get(): any
  }

  export class Property {
    public value

    constructor(thing: Thing, name: string, value: Value, metadata: object)

    public getThing(): Thing
    public getName(): string
    public getValue(): any
    public setValue(value: any)

    public asDescription(): string
  }

  export class LabelProperty extends Property {
    constructor(thing: Thing, name: string, value: Value)
  }

  export class Event {
    public data

    constructor(thing: Thing, name: string, data?: object)
  }

  export class Thing {
    constructor(name: string, type: string, description: string)

    public addProperty(p: Property): void
    public setProperty(name: string, value: any): void
    public addSubscriber(subs: Observer)

    public addAvailableEvent(name: string, metadata: object)
    public addEventSubscriber(name: string, subs: Observer)
    public addEvent(event: Event)

    public addAvailableAction(name: string, metadata: object, cls: any)
    public performAction(name: string, input: object)

    public asDescription(): string
  }
}
