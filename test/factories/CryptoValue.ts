import { expect } from 'chai'
import { Value } from 'quodii-lib'
import CryptoValueFactory from '../../src/factories/CryptoValue'
import CryptoValue from '../../src/lib/CryptoValue'

describe('factories > CryptoValue', () => {
// tslint:disable-next-line: no-empty
  const device = new Value(null, () => {})
  const factory = CryptoValueFactory()
  const fixtures: Array<{ args: {}, expected: any }> = [
    { args: { device , symbol: 'BTC' }, expected: { klass: CryptoValue }}
  ]

  fixtures.forEach(({ args, expected }) => {
    describe(`when call with ${JSON.stringify(args)}`, () => {
      const result = factory(args)

      it('should return instance of "CryptoValue"', () => {
        expect(result).to.be.instanceOf(expected.klass)
      })
    })
  })
})
