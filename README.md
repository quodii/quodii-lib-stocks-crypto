# Quodii Crypto Currency Stocks Library


## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and docker installed.

```sh
$ npm install
```

### Build project
```sh
$ npm run build
```

### Run Linter
```sh
$ npm run linter
```
