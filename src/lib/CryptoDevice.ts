import { PoolingSensorValue } from 'quodii-lib'
import fetch from 'axios'
import Logger from './Logger'

export default class CryptoDevice extends PoolingSensorValue {
  private url: string
  private logger: Logger

  constructor(symbols: string[], currencies: string[] = ['USD'], poolInterval: number = 10000) {
    super(poolInterval)

    this.logger = new Logger('CryptoDevice')
    this.url = `https://min-api.cryptocompare.com/data/pricemulti?fsyms=${symbols.join(',')}&tsyms=${currencies.join(',')}`
  }

  /**
   * Mimic an actual sensor updating its reading every couple seconds.
   */
  protected read(): Promise<any> {
    return Promise.resolve(this.url)
      .then((url) => fetch.get(url))
      .then((response) => response.data)
      .catch(this.logger.error)
  }
}
