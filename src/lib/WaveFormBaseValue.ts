import Logger from './Logger'
import { PoolingSensorValue } from 'quodii-lib'

export default abstract class WaveFormBaseValue extends PoolingSensorValue {

  protected counter: number
  protected start: number
  protected step: number
  protected logger: Logger

  constructor(poolInterval: number, step: number = 1, start: number = 0) {
    super(poolInterval)

    this.start = start
    this.step = step
    this.logger = new Logger('WaveFormBaseValue')
  }

  /**
   * Mimic an actual sensor updating its reading every couple seconds.
   */
  protected read(): Promise<number> {
    if (this.counter === undefined) {
      this.counter = this.start
    } else {
      this.counter += this.step
    }

    this.logger.debug(this.counter, this.step)

    return Promise.resolve(this.counter)
  }
}
