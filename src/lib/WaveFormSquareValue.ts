import { PoolingSensorValue } from 'quodii-lib'
import WaveFormSawtoothValue from './WaveFormSawtoothValue'

export default class WaveFormSquareValue extends WaveFormSawtoothValue {

  /**
   * Mimic an actual sensor updating its reading every couple seconds.
   */
  protected read(): Promise<number> {
    return super.read().then((value: number) => {
      if (value < 0) {
        value = this.amplitude * -1
      } else {
        value = this.amplitude
      }

      return value
    })
  }
}
