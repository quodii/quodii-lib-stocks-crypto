import CryptoDevice from '../lib/CryptoDevice'

export = () => {
  return (data: any): CryptoDevice => {
    const { symbols = [], currencies = ['USD'], poolInterval = 30000} = data || {}
    return new CryptoDevice(symbols, currencies, poolInterval)
  }
}
