import CryptoValue from '../lib/CryptoValue'
import assert from 'assert'

export = () => {
  return (data: any): CryptoValue => {
    const { device = null, symbol = null, currency = 'USD' } = data || {}
    assert(device, 'CryptoValue Error: "device" should be defined')
    assert(symbol, 'CryptoValue Error: "symbol" should be defined')

    return new CryptoValue(device, symbol, currency)
  }
}
